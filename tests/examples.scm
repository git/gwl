;;; Copyright © 2020, 2021, 2022 Ricardo Wurmus <rekado@elephly.net>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (test-examples)
  #:use-module (gwl ui)
  #:use-module (gwl utils)
  #:use-module (gwl errors)
  #:use-module ((guix build utils)
                #:select (delete-file-recursively))
  #:use-module (srfi srfi-64))

(define user-module-for-file
  (@@ (gwl workflows utils) user-module-for-file))

(define wisp-reader
  (@@ (gwl workflows utils) wisp-reader))

(define read-wisp
  (lambda (port)
    (wisp-reader port (user-module-for-file "test.w"))))

(define top-srcdir (getenv "abs_top_srcdir"))

(define-syntax-rule (with-temporary-directory prefix body body* ...)
  (let ((old-dir (getcwd))
        (tmp-dir (mkdtemp
                  (format #f "/tmp/~a.XXXXXX" prefix))))
    (dynamic-wind
      (lambda ()
        (log-event 'info (G_ "Changing directory: ~a") tmp-dir)
        (chdir tmp-dir))
      (lambda () body body* ...)
      (lambda ()
        (log-event 'info (G_ "Changing directory: ~a") old-dir)
        (chdir old-dir)
        (delete-file-recursively tmp-dir)))))

(define (process-success? status)
  (zero? (or (status:exit-val status)
             (status:term-sig status))))

(define* (run-example example #:key (engine "simple") )

  (define abs-example
    (canonicalize-path
     (string-append top-srcdir "/doc/examples/" example)))

  (with-temporary-directory
   (string-append "gwl-example-" example)
   (process-success?
    (apply system* "guix" "workflow"
           `("run"
             ,(string-append "--engine=" engine "-engine")
             "--force"
             ,@(cond
                ((string=? "simple" engine)
                 '("--container"))
                (else '()))
             "--log-events=all"
             ,abs-example)))))

(define-syntax-rule (test-example example)
  (begin
    (when (getenv "GWL_SKIP_INTEGRATION_TESTS")
      (test-skip 1))
    (test-assert (string-append example " [simple]")
      (run-example example))
    (when (or (getenv "GWL_SKIP_INTEGRATION_TESTS")
              (getenv "GWL_SKIP_DRMAA_TESTS"))
      (test-skip 1))
    (test-assert (string-append example " [drmaa]")
      (run-example example #:engine "drmaa"))))

(test-begin "examples")

(test-equal "wisp syntax produces the expected S-expression"
  '(process haiku
            (outputs "haiku.txt")
            (synopsis "Write a haiku to a file")
            (description "This process writes a haiku by Gary Hotham to the file \"haiku.txt\".")
            (procedure (quasiquote
                        (with-output-to-file (unquote outputs)
                          (lambda () (display "the library book\noverdue?\nslow falling snow"))))))

  (call-with-input-file (string-append top-srcdir
                                       "/doc/examples/haiku.w")
    read-wisp))

(test-example "extended-example-workflow.w")
(test-example "extended-example-workflow.scm")
(test-example "example-workflow.w")
(test-example "example-workflow.scm")
(test-example "simple-wisp.w")
(test-example "simple.scm")

(test-end "examples")
