;;; Copyright © 2017, 2018 Roel Janssen <roel@gnu.org>
;;; Copyright © 2018-2024 Ricardo Wurmus <rekado@elephly.net>
;;;
;;; This program is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gwl processes)
  #:use-module (oop goops)
  #:use-module (gwl errors)
  #:use-module (gwl oop)
  #:use-module (gwl process-engines)
  #:use-module (gwl packages)
  #:use-module (gwl ui)
  #:use-module ((guix profiles)
                #:select
                (profile
                 manifest manifest?
                 manifest-search-paths
                 packages->manifest
                 concatenate-manifests))
  #:use-module ((guix search-paths)
                #:select
                (search-path-specification->sexp))
  #:use-module (guix gexp)
  #:use-module ((guix modules)
                #:select (source-module-closure))
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-34)
  #:use-module (srfi srfi-35)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 threads)
  #:use-module (ice-9 rdelim)
  #:export (make-process
            process?
            process-name
            process-full-name
            process-version
            process-packages
            process-raw-inputs
            process-inputs
            process-run-time
            process-procedure
            process-synopsis
            process-description

            process-manifest
            process-outputs
            process-output-path
            process-takes-available
            print-process-record

            complexity
            complexity-space
            complexity-time
            complexity-threads

            process->script
            process->script-arguments
            process->script-wrapper

            ;; Convenience functions
            kibibytes
            mebibytes
            gibibytes

            KiB
            MiB
            GiB

            seconds
            minutes
            hours

            process-space
            process-time
            process-threads

            processes-filter
            processes-filter-by-name

            code-snippet
            code-snippet?
            code-snippet-language
            code-snippet-arguments
            code-snippet-code

            compile-procedure
            run-process-command

            containerize))

;;; Commentary:
;;;
;;; This module provides a high-level mechanism to define processes in a
;;; Guix-based distribution.

;;; ---------------------------------------------------------------------------
;;; RECORD TYPES
;;; ---------------------------------------------------------------------------

;; A `complexity' is a way of describing the run-time complexity of a
;; `process'.
(define-class <complexity> (<gwl-class>)
  (threads
   #:init-value 1
   #:init-keyword #:threads
   #:accessor complexity-threads)
  (space
   #:init-value #f
   #:init-keyword #:space
   #:accessor complexity-space)
  (time
   #:init-value #f
   #:init-keyword #:time
   #:accessor complexity-time))

;; Convenient DSL-like constructor.
(define-syntax complexity
  (lambda (x)
    (syntax-case x ()
      ((_ fields ...)
       #`(let* (#,@(map (lambda (field)
                          (syntax-case field ()
                            ((empty-field)
                             (syntax-violation #f "complexity: Empty field" #'empty-field))
                            ((field n unit)
                             #'(field (unit n)))
                            ((field single-value)
                             #'(field single-value))))
                        #'(fields ...)))
           (make <complexity>
             #,@(append-map (lambda (field)
                              (syntax-case field ()
                                ((name . rest)
                                 #`((symbol->keyword 'name) name))))
                            #'(fields ...))))))))

(define (complexity? thing)
  (is-a? thing <complexity>))

(define (print-complexity complexity port)
  "Write a concise representation of COMPLEXITY to PORT."
  (simple-format port "<#complexity ~a sec, ~a bytes, ~a threads>"
                 (complexity-time complexity)
                 (complexity-space complexity)
                 (complexity-threads complexity)))

(define-method (write (complexity <complexity>) port)
  (print-complexity complexity port))


;; A process is a stand-alone unit doing some work.  This is typically a
;; single program, configured through command-line options, given a certain
;; input, producing a certain output.

(define-class <process> (<gwl-class>)
  ;; Slots
  (name
   #:accessor process-name
   #:init-keyword #:name
   #:implicit-concatenation? #t
   #:required? #t)
  (version
   #:accessor process-version
   #:init-keyword #:version
   #:init-value #f)
  (synopsis
   #:accessor process-synopsis
   #:init-keyword #:synopsis
   #:init-value "")
  (description
   #:accessor process-description
   #:init-keyword #:description
   #:init-value "")
  (packages
   #:accessor process-packages
   #:init-keyword #:packages
   #:init-form (manifest '())
   #:implicit-list? #t
   #:validator manifest?
   #:transformer
   ;; TODO: the instance name is not be available at this point, so we
   ;; can't report the process name here.  We should move the
   ;; transformers and validators to a point after initialization.
   (match-lambda*
     ((_ (? manifest? value)) value)
     ((_ packages)
      (packages->manifest
       (map
        (match-lambda
          ((? string? spec)
           (let ((pkg out (lookup-package spec)))
             (list pkg out)))
          ((? valid-package? pkg)
           pkg)
          (((? valid-package? pkg) (? string? out))
           (list pkg out))
          (x
           (raise
            (condition
             (&gwl-type-error
              (expected-type (list "<package>" "<inferior-package>" "<string>"))
              (actual-value x))))))
        packages)))))
  (inputs
   #:accessor process-raw-inputs
   #:init-keyword #:inputs
   #:init-value '()
   #:implicit-list? #t)
  (output-path
   #:accessor process-output-path
   #:init-keyword #:output-path
   #:init-value #f)
  (outputs
   #:accessor process-outputs*
   #:init-keyword #:outputs
   #:init-value '()
   #:implicit-list? #t
   #:validator list?)
  (run-time
   #:accessor process-run-time
   #:init-keyword #:run-time
   #:init-value #f
   #:validator complexity?)
  (values
   #:accessor process-values
   #:init-keyword #:values
   #:init-value '()
   #:implicit-list? #t)
  (procedure
   #:accessor process-procedure
   #:init-keyword #:procedure
   #:required? #t
   #:validator (lambda (value)
                 (or (code-snippet? value)
                     (list? value)
                     (gexp? value)
                     (raise (condition
                             (&gwl-error)
                             (&formatted-message
                              (format "unsupported procedure: ~a~%")
                              (arguments (list value))))))))
  ;; Class options
  #:name "process")

(define (process? thing)
  (is-a? thing <process>))

(define (flat-list . items)
  (fold (lambda (item acc)
          (match item
            ((? list? ls)
             (append acc ls))
            (_ (append acc (list item)))))
        '() items))

;; This is a constructor for <process> instances.  It permits the use
;; of multiple field values (implicit lists) and cross-field
;; references.  It does not, however, validate any fields or their
;; values.
;; TODO: support "inherit" syntax.
(define-syntax make-process
  (lambda (x)
    (syntax-case x ()
      ((_ fields ...)
       ;; If the last field is a plain code snippet produced with
       ;; special syntax, add the field name.
       (let ((fields* (match (reverse #'(fields ...))
                        ((last-field before ___)
                         (match (syntax->datum last-field)
                           ;; Wisp rules let the code-snippet be
                           ;; wrapped in an extra pair of parens
                           ;; unless we start the line with a dot.
                           ((('code-snippet . _))
                            (append (reverse before)
                                    (list #`(procedure #,@last-field))))
                           (('code-snippet . _)
                            (append (reverse before)
                                    (list #`(procedure #,last-field))))
                           (_ #'(fields ...)))))))
         #`(let* (#,@(map (lambda (field)
                            (syntax-case field ()
                              ((empty-field)
                               (syntax-violation #f "process: Empty field" #'empty-field))
                              ((key value)
                               ;; No valid fields are keywords.
                               ;; Signal this error early instead of
                               ;; constructing an invalid let*.
                               (when (keyword? (syntax->datum #'key))
                                 (syntax-violation #f "process: Invalid field name" #'key))
                               #'(key value))
                              ((key values ...)
                               ;; No valid fields are keywords.
                               ;; Signal this error early instead of
                               ;; constructing an invalid let*.
                               (when (keyword? (syntax->datum #'key))
                                 (syntax-violation #f "process: Invalid field name" #'key))

                               ;; XXX: This is a crude way to allow
                               ;; for definitions inside of field
                               ;; values.
                               (let ((definition?
                                       (lambda (token)
                                         (string-prefix? "define"
                                                         (symbol->string token)))))
                                 (match (syntax->datum #'(values ...))
                                   ((((? definition? token) . _) . _)
                                    ;; Start a definition context
                                    #'(key (let context () (begin values ...))))
                                   (_ #'(key (flat-list values ...))))))))
                          fields*))
             (make <process>
               #,@(append-map (lambda (field)
                                (syntax-case field ()
                                  ((name . rest)
                                   #`((symbol->keyword 'name) name))))
                              fields*))))))))

(define (print-process process port)
  "Write a concise representation of PROCESS to PORT."
  (simple-format port "#<process ~a>" (process-full-name process)))

(define* (print-process-record process #:optional (port #t))
  "Write a multi-line representation of PROCESS to PORT."
  (format port "name: ~a~%version: ~a~%synopsis: ~a~%description: ~a~%~%"
          (process-name process)
          (process-version process)
          (process-synopsis process)
          (process-description process)))

(define (process-full-name process)
  "Returns the name and version of PROCESS."
  (if (process-version process)
      (string-append (process-name process) "-"
                     (process-version process))
      (process-name process)))

(define-method (write (process <process>) port)
  (print-process process port))


;;; Support for embedding foreign language snippets
(define-class <language> ()
  (name
   #:init-keyword #:name
   #:accessor language-name) ; symbol
  (call
   #:init-keyword #:call
   #:accessor language-call)) ; procedure

(define (process->env process)
  "Return an alist of environment variable names to values of fields
of PROCESS."
  `(("_GWL_PROCESS_NAME" . name)
    ("_GWL_PROCESS_SYNOPSIS" .
     ,(or (process-synopsis process) ""))
    ("_GWL_PROCESS_DESCRIPTION" .
     ,(or (process-description process) ""))
    ("_GWL_PROCESS_INPUTS" . inputs)
    ("_GWL_PROCESS_OUTPUT_PATH" .
     ,(or (process-output-path process) ""))
    ("_GWL_PROCESS_OUTPUTS" . outputs)
    ("_GWL_PROCESS_VALUES" . values)
    ("_GWL_PROCESS_COMPLEXITY_THREADS" .
     ,(or (and=> (process-threads process) number->string) ""))
    ("_GWL_PROCESS_COMPLEXITY_SPACE" .
     ,(or (and=> (process-space process) number->string) ""))
    ("_GWL_PROCESS_COMPLEXITY_TIME" .
     ,(or (and=> (process-time process) number->string) ""))))

(define (snippet-caller code-converter command)
  (lambda (process code)
    `(begin
       (use-modules (srfi srfi-1))
       (for-each (lambda (pair)
                   (setenv (car pair)
                           (let ((value (cdr pair)))
                             (if (symbol? value)
                                 (format #false "~a"
                                         (assoc-ref #{ %gwl process-arguments}# value))
                                 value))))
                 ',(process->env process))
       (let ((retval (apply system*
                            (append ,command
                                    (,code-converter (string-join (map (lambda (val)
                                                                         (if (list? val)
                                                                             (format #f "~{~a~^ ~}" val)
                                                                             (format #f "~a" val)))
                                                                       (list ,@code))
                                                                  ""))))))
         (or (zero? retval) (exit retval))))))

(define language-python
  (make <language>
    #:name 'python
    #:call (snippet-caller 'list '(list "python3" "-c"))))

(define language-r
  (make <language>
    #:name 'R
    #:call (snippet-caller
            '(lambda (code)
               (append-map (lambda (line)
                             (list "-e" line))
                           (filter (negate string-null?)
                                   (string-split code #\newline))))
            '(list "Rscript"))))

(define language-bash
  (make <language>
    #:name 'bash
    #:call (snippet-caller 'list '(list "bash" "-c"))))

(define language-sh
  (make <language>
    #:name 'sh
    #:call (snippet-caller 'list '(list "/bin/sh" "-c"))))

(define languages
  (list language-sh
        language-bash
        language-python
        language-r))

(define-class <code-snippet> (<applicable-struct>)
  (language
   #:init-keyword #:language
   #:accessor code-snippet-language)
  (arguments
   #:init-keyword #:arguments
   #:accessor code-snippet-arguments)
  (code
   #:init-keyword #:code
   #:accessor code-snippet-code))

;; Code snippets should be evaluating to themselves for more
;; convenient Wisp use.
(define-method (initialize (self <code-snippet>) initargs)
  (next-method self
               (append (list #:procedure (lambda args self))
                       initargs)))

(define (code-snippet? thing)
  (is-a? thing <code-snippet>))

(define (code-snippet language arguments code)
  (make <code-snippet>
    #:language language
    #:arguments arguments
    #:code code))

(define (compile-procedure process)
  "Transform the procedure of PROCESS to an expression that can be
written to a file."
  ;; TODO: replace with normalize-file-name in utils?
  (define (sanitize-path path)
    (string-join (delete ".." (string-split path #\/))
                 "/"))
  (match (process-procedure process)
    ((? gexp? g) g)
    ((? list? s) s)
    ((? code-snippet? snippet)
     (let* ((name (code-snippet-language snippet))
            (code (code-snippet-code snippet))
            (call (or (and=> (find (lambda (lang)
                                     (eq? name (language-name lang)))
                                   languages)
                             language-call)
                      ;; There is no pre-defined way to execute the
                      ;; snippet.  Use generic approach.
                      (snippet-caller 'list
                                      `(list
                                        (string-append (getenv "_GWL_PROFILE")
                                                       ,(sanitize-path (symbol->string name)))
                                        ,@(code-snippet-arguments snippet))))))
       (call process code)))))

(define* (call-with-output-processor command proc #:optional capture-stderr?)
  "Silently execute COMMAND, a list of strings representing an
executable with its arguments, and apply PROC to every line printed to
standard output and, optionally when CAPTURE-STDERR? is #T, standard
error.  Return the exit status of COMMAND."
  ;; We can only capture a program's standard error by parameterizing
  ;; current-error-port to a *file* port before using system* or
  ;; open-pipe*.  The process will write its standard error stream to
  ;; the provided file descriptor.  Meanwhile we read from the file
  ;; descriptor (blocking) for new lines until the process exits.
  (match (socketpair PF_UNIX SOCK_STREAM 0)
    ((in . out)
     (let ((err (if capture-stderr?
                    (dup out)
                    (%make-void-port "w"))))
       (catch #true
         (lambda ()
           (let ((thread
                  (parameterize ((current-error-port err)
                                 (current-output-port out))
                    (call-with-new-thread
                     (lambda ()
                       (let ((status (apply system* command)))
                         (close-port err)
                         (close-port out)
                         status))))))
             (let loop ()
               (match (read-line in 'concat)
                 ((? eof-object?)
                  (join-thread thread))
                 (line
                  (proc line)
                  (loop))))))
         (lambda (key . args)
           (for-each
            (lambda (port)
              (false-if-exception (close-port port)))
            (list err out in))
           (apply throw key args)))))))

(define (run-process-command command)
  "Run the COMMAND, a list consisting of an executable and its
arguments.  Log any output on the standard output or standard error
streams as a 'process event."
  (call-with-output-processor
   command
   (cut log-event 'process <>)
   'capture-stderr))


(define* (script-modules #:optional containerize?)
  (if containerize?
      '((gnu build accounts)
        (gnu build linux-container)
        (gnu system file-systems)
        (guix build utils)
        (guix search-paths)
        (ice-9 match))
      '((guix search-paths))))

(define (containerize script)
  "Call SCRIPT, a program-file object, in a G-expression that sets up a
container where the provided inputs are available (in addition to all
store items needed for execution).  All outputs are copied outside of
the container."
  (with-imported-modules (source-module-closure
                          (script-modules 'container))
    #~(begin
        (use-modules (gnu build accounts)
                     (gnu build linux-container)
                     (gnu system file-systems)
                     (guix build utils)
                     (ice-9 match))
        (define (location->file-system source target writable?)
          (file-system
            (device source)
            (mount-point target)
            (type "none")
            (flags (if writable?
                       '(bind-mount)
                       '(bind-mount read-only)))
            (check? #f)
            (create-mount-point? #t)))
        (let* ((pwd (getpw))
               (uid (getuid))
               (gid (getgid))
               (passwd (let ((pwd (getpwuid (getuid))))
                         (password-entry
                          (name (passwd:name pwd))
                          (real-name (passwd:gecos pwd))
                          (uid uid) (gid gid) (shell "/bin/sh")
                          (directory (passwd:dir pwd)))))
               (groups (list (group-entry (name "users") (gid gid))
                             (group-entry (gid 65534) ;the overflow GID
                                          (name "overflow")))))
          (match (command-line)
            ((_ (= (lambda (s) (call-with-input-string s read))
                   #{ %gwl process-arguments}#) . rest)
             (call-with-container
                 (append %container-file-systems
                     ;; Current directory for final outputs
                     (list (location->file-system
                            (canonicalize-path ".") "/gwl" #t))
                   (map (lambda (location)
                          (location->file-system location location #f))
                        (append (call-with-input-file #$(references-file script) read)
                            (assoc-ref #{ %gwl process-arguments}# 'inputs))))
               (lambda ()
                 (unless (file-exists? "/bin/sh")
                   (unless (file-exists? "/bin")
                     (mkdir "/bin"))
                   (symlink #$(file-append (bash-minimal) "/bin/sh") "/bin/sh"))

                 ;; Setup directory for temporary files.
                 (mkdir-p "/tmp")
                 (for-each (lambda (var)
                             (setenv var "/tmp"))
                           '("TMPDIR" "TEMPDIR"))

                 ;; Create a dummy /etc/passwd to satisfy applications that demand
                 ;; to read it.
                 (unless (file-exists? "/etc")
                   (mkdir "/etc"))
                 (write-passwd (list passwd))
                 (write-group groups)

                 (apply system* #$script (cdr (command-line)))

                 ;; Copy generated files to final directory.
                 (for-each (lambda (output)
                             (let ((target (string-append "/gwl/" output)))
                               (mkdir-p (dirname target))
                               (copy-file output target)))
                           (filter file-exists?
                                   (assoc-ref #{ %gwl process-arguments}# 'outputs))))
               #:guest-uid uid
               #:guest-gid gid)))))))

;;; ---------------------------------------------------------------------------
;;; ADDITIONAL FUNCTIONS
;;; ---------------------------------------------------------------------------

(define* (process-inputs process #:optional with-tags?)
  "Return the plain values of all inputs of PROCESS, without any
keyword tags if WITH-TAGS? is #FALSE or missing."
  (if with-tags?
      (process-raw-inputs process)
      (remove keyword? (process-raw-inputs process))))

(define* (process-outputs proc #:optional with-tags?)
  "Return the output location(s) of process PROC, without any keyword
tags if WITH-TAGS? is #FALSE or missing."
  (let* ((root (process-output-path proc))
         (mangle (if root
                     (cut string-append root "/" <>)
                     identity)))
    (if with-tags?
        (map mangle (process-outputs* proc))
        (map mangle (remove keyword? (process-outputs* proc))))))

(define (process-manifest process)
  (concatenate-manifests
   ;; Put process packages before bash-minimal, so that
   ;; they're not shadowed.
   (list (process-packages process)
         (packages->manifest (list (bash-minimal))))))

(define (process-takes-available process)
  "Returns #T when the data inputs of the PROCESS exist."
  (match (process-inputs process)
    ((? list? inputs)
     (every file-exists? inputs))
    (_ #t)))

;;; ---------------------------------------------------------------------------
;;; DERIVATIONS AND SCRIPTS FUNCTIONS
;;; ---------------------------------------------------------------------------

;; Taken from (gnu services base)
(define* (references-file item #:optional (name "references"))
  "Return a file that contains the list of references of ITEM."
  (if (struct? item)                              ;lowerable object
      (computed-file name
                     (with-extensions (list (lookup-package "guile-gcrypt")) ;for store-copy
                       (with-imported-modules (source-module-closure
                                               '((guix build store-copy)))
                         #~(begin
                             (use-modules (guix build store-copy))

                             (call-with-output-file #$output
                               (lambda (port)
                                 (write (map store-info-item
                                             (call-with-input-file "graph"
                                               read-reference-graph))
                                        port))))))
                     #:options `(#:local-build? #false
                                 #:references-graphs (("graph" ,item))))
      (plain-file name "()")))

(define (process->script-arguments process)
  `((inputs  . ,(process-inputs process #:with-tags))
    (outputs . ,(process-outputs process #:with-tags))
    (values  . ,(process-values process))
    (name    . ,(process-name process))))

(define* (process->script process
                          #:key
                          containerize?
                          workflow
                          (input-files '()))
  "Return a lowerable object for the script that will execute the
PROCESS."
  (let* ((name         (process-full-name process))
         (manifest     (process-manifest process))
         (profile      (profile (content manifest)))
         (search-paths (delete-duplicates
                        (map search-path-specification->sexp
                             (manifest-search-paths manifest))))
         (out          (process-output-path process))
         (exp
          (with-imported-modules (source-module-closure (script-modules))
            #~(begin
                (use-modules (guix search-paths))
                (set-search-paths (map sexp->search-path-specification
                                       '#$search-paths)
                                  (list #$profile))
                #$(if out `(setenv "out" ,out) "")
                (setenv "_GWL_PROFILE" #$profile)
                (use-modules (ice-9 match))
                (match (command-line)
                  ((_ (= (lambda (s) (call-with-input-string s read))
                         #{ %gwl process-arguments}#) . rest)
                   #$(compile-procedure process))))))
         (script
          (program-file (string-append "gwl-" name ".scm")
                        exp
                        #:guile (default-guile)))
         (computed-script
          (if containerize?
              (program-file (string-append "gwl-" name "-container.scm")
                            (containerize script)
                            #:guile (default-guile))
              script)))
    computed-script))

(define* (process->script-wrapper process
                                  #:key make-wrapper workflow scripts-table)
  (make-wrapper process
                (hashq-ref scripts-table process)
                #:workflow workflow
                #:scripts-table scripts-table))

;;; ---------------------------------------------------------------------------
;;; CONVENIENCE FUNCTIONS
;;; ---------------------------------------------------------------------------

(define (kibibytes number)
  (* number 1024))
(define (mebibytes number)
  (* (kibibytes number) 1024))
(define (gibibytes number)
  (* (mebibytes number) 1024))

(define KiB kibibytes)
(define MiB mebibytes)
(define GiB gibibytes)

(define seconds identity)
(define (minutes number)
  (* number 60))
(define (hours number)
  (* (minutes number) 60))

(define (process-space process)
  (and=> (process-run-time process) complexity-space))

(define (process-time process)
  (and=> (process-run-time process) complexity-time))

(define (process-threads process)
  (and=> (process-run-time process) complexity-threads))

(define (processes-filter processes filter)
  "Returns a list of PROCESSES after applying FILTER.  FILTER
is a function that takes a process and returns the process to include it
or #f to exclude it."
  (filter-map filter processes))

(define (processes-filter-by-name processes name)
  "Returns a list of PROCESSES whose name (partially) matches NAME."
  (filter-map (lambda (proc)
                (and (process? proc)
                     (string-contains (process-name proc) name)
                     proc))
              processes))
